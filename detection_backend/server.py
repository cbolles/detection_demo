from flask import Flask
from flask import request
from flask_cors import CORS
import sys
import os
cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(cwd, 'ap_rd_ssd/'))
from ap_rd_ssd import process
from multiprocessing import Process, Value, Queue

app = Flask(__name__)
CORS(app)

analysis_mode = Value('i', 1)
video_source = Queue(maxsize=2)
analysis_mode.value = 3

@app.route('/sourceFeed')
def source_feed():
    video_source.put(request.args.get('source'))
    analysis_mode.value = 1
    return 'Starting Feed with source ' + request.args.get('source')

@app.route('/startFeed')
def start_feed():
    analysis_mode.value = 1
    return 'Starting Feed'

@app.route('/pauseFeed')
def pause_feed():
    analysis_mode.value = 2
    return 'Pausing Feed'

@app.route('/stopFeed')
def stop_feed():
    analysis_mode.value = 3
    return 'Stopping Feed'

def flask_process():
    app.run()

process_flask = Process(target=flask_process)
process_flask.start()
while True:
    while analysis_mode.value == 3:
        pass
    properties = process.default_properties()
    properties['tensor_graph'] = 'ap_rd_ssd/duck_good.pb'
    properties['tensor_labels'] = 'ap_rd_ssd/duck.pbtxt'
    properties['video_source'] = video_source.get()
    #For using web cam video source
    if properties['video_source'] == '0' or properties['video_source'] == '':
        properties['video_source'] = 0
    process.process_control(properties, analysis_mode)
