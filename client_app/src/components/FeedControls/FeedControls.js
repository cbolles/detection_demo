import React, { Component } from 'react';
import '../../App.css';
import './FeedControls.css';
import fetch from 'cross-fetch';

export class FeedControls extends Component {

  sourceFeed() {
    const source = document.getElementById('sourceFeed').value;
    console.log(source);
    fetch("http://localhost:5000/sourceFeed?source=" + source)
      .then(res => console.log(res))
  }

  startFeed() {
    fetch("http://localhost:5000/startFeed")
      .then(res => console.log(res))
  }

  pauseFeed() {
    fetch("http://localhost:5000/pauseFeed")
      .then(res => console.log(res))
  }

  stopFeed() {
    fetch("http://localhost:5000/stopFeed")
      .then(res => console.log(res))
  }

  render() {
    return (
      <div className="FeedSection">
        <label>Source</label><input type="text" id="sourceFeed"/>
        <button onClick={this.sourceFeed}>Start</button>
        <div className="btn-group">
          <button className="button" onClick={this.startFeed}>&#9658; Play Feed Process</button>
          <button className="button" onClick={this.pauseFeed}>&#9613;&#9613; Pause Feed</button>
          <button className="button" onClick={this.stopFeed}>&#9607; Stop Feed</button>
        </div>
      </div>
    );
  }
}
