import React, { Component } from 'react';
import { FeedView } from './components/FeedView/FeedView.js';
import { FeedControls } from './components/FeedControls/FeedControls.js';
import { ListenerResults } from './components/ListenerResults/ListenerResults.js'
import logo from './logo.png';
import './App.css';
import { Col, Grid, Row } from 'react-bootstrap';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Detection Demo</h1>
        </header>
        <Grid>
          <Row>
            <Col xs={6} md={1}>
              <FeedControls />
            </Col>
            <Col xs={12} md={11}>
              <FeedView />
            </Col>
          </Row>
          <Row>
            <ListenerResults />
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
